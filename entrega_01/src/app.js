require('bootstrap');
import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import useToken from './useToken';

import Register from './components/register.js';
import Login from './components/login.js';
import Game from './components/game.js';
import { ToastProvider, useToasts } from 'react-toast-notifications'

function App() {
    const { token, setToken } = useToken();

    if (!token && window.location.pathname !== "/register") {

        return <BrowserRouter><Login setToken={setToken} /></BrowserRouter>
    }
    else {
        return (
            <BrowserRouter>
                <Route exact path="/" component={Game} />
                <Route path="/login" component={() => <Login setToken={setToken} />} />

                <Route path="/register" component={Register} />
                <Route path="/game" component={Game} />
            </BrowserRouter>
        );
    }

}
export default App;