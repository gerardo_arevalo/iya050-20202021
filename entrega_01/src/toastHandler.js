import { useToasts } from 'react-toast-notifications';
import { useState } from 'react';

export default function useToaster(response) {
    const { addToast } = useToasts()
    const [toastResult, setToastResult] = useState(false);
    if (response.ok) {
        setResult(true)
        if (response.status == 201) {
            addToast('User was registered!', {
                appearance: 'success',
                autoDismiss: true,
                pauseonhover: "true",
            })
        }
    } else {
        setResult(false)
        if (response.status == 409) {
            addToast(response.body.details, {
                appearance: 'error',
                autoDismiss: true,
                pauseonhover: "true",
            })
        }
        else {
            addToast('Por favor intente de nuevo', {
                appearance: 'error',
                autoDismiss: true,
                pauseonhover: "true",
            })
        }

    }
    return {
        toastResult
    }
}