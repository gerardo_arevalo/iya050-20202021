import React from 'react';
import ReactDom from 'react-dom';
import App from './app';
// Import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastProvider } from 'react-toast-notifications'

ReactDom.render(<ToastProvider placement="bottom-right"><App /></ToastProvider>, document.getElementById("root"))