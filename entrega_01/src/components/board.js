import React, { useState, useEffect } from "react";
import '../styles/spinner.css'
import Hand from './hand'

function Board({ gameState }) {

    return (
        <div className="board">
            <h3 className="playername">
                {gameState.player}
            </h3>
            <Hand cards={gameState.playerCards} player="player" />
            <h3 className="playername">
                CPU
            </h3>
            <Hand cards={gameState.cpuCards} player="cpu" />
        </div >
    );

}

export default Board;