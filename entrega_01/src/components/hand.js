import React, { useState, useEffect } from "react";
import { NavItem } from "react-bootstrap";
import '../styles/spinner.css'

function Hand({ cards, player }) {
    const hand = (
        <div className={"player " + player}>{cards.map((item) => <img className={"Card " + item.cardName} src={item.url} />)}</div> // `.map()` creates/returns a new array from calling a function on every element in the array it's called on
    )
    return (
        hand
    );

}

export default Hand;