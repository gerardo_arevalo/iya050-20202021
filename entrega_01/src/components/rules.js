import { deck } from './cards'

const validStartCard = () => {
    let item = deck[Math.floor(Math.random() * deck.length)];
    while (
        (item.cardName === "Draw4") |
        (item.cardName === "ChangeColor") |
        (item.cardName === "RedSkip") |
        (item.cardName === "RedDraw2") |
        (item.cardName === "RedReverse") |
        (item.cardName === "BlueSkip") |
        (item.cardName === "BlueDraw2") |
        (item.cardName === "BlueReverse") |
        (item.cardName === "YellowSkip") |
        (item.cardName === "YellowDraw2") |
        (item.cardName === "YellowReverse") |
        (item.cardName === "GreenSkip") |
        (item.cardName === "GreenDraw2") |
        (item.cardName === "GreenReverse")
    ) {
        item = deck[Math.floor(Math.random() * deck.length)];
    }
    return item;
};

function startGame(user) {


    let cards1 = Array.from(
        { length: 7 },
        () => deck[Math.floor(Math.random() * deck.length)]
    );
    let cardsCpu = Array.from(
        { length: 7 },
        () => deck[Math.floor(Math.random() * deck.length)]
    );
    return (
        {
            player: user,
            playerCards: cards1,
            cpuCards: cardsCpu,
            cardPlaying: validStartCard(),
        }
    );
}

const isValidCard = (item, gameState) => {
    const arr1 = gameState.cardPlaying.number;
    const arr2 = gameState.cardPlaying.color;

    if (
        arr1.some(i => item.number.includes(i)) ||
        arr2.some(i => item.color.includes(i))
    )
        return true;
    else return false;
};

const removeCard = (player, item) => {
    let cards = document.getElementsByClassName(player)[0];
    let card = cards.getElementsByClassName(item.cardName)[0];
    card.textContent = "";
    card.remove();
};

const getCardFromDeck = state => {
    let item = deck[Math.floor(Math.random() * deck.length)];
    let player;
    if (state.turn) {
        state.playerCards.push(item);
        state.turn = false
    }
    else {
        state.cpuCards.push(item);
        state.turn = true
    }
    return state;
};

const playCard = (item, gameState) => {
    if (isValidCard(item, gameState)) {


        if (gameState.turn == true) {
            gameState.cardPlaying = item;
            const indx = gameState.playerCards.findIndex(
                v => v.cardName === item.cardName
            );
            gameState.playerCards.splice(indx, 1);
            gameState.turn = false;
            return gameState;
        }
    } else {
        return "Carta no jugable";
    }
};

export { startGame, getCardFromDeck };