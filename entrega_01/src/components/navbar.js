import React from 'react';
import { useHistory, NavLink, withRouter, useLocation } from 'react-router-dom'
import useToken from '../useToken';
import { useToasts } from 'react-toast-notifications';
import '../styles/spinner.css'

let getNavLinkClass = (path) => {
    const location = useLocation();
    React.useEffect(() => {
        return location.pathname === path ? 'active' : '';
    }, [location]);

};

async function logout(token) {

    //console.log(JSON.stringify(credentials.password))
    const response = await fetch('https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/' + token.token, {
        method: 'DELETE',
        headers: {
            'Authorization': token.token
        }

    }).then(res => ({ ok: res.ok, status: res.status, body: res.body }))

    return response;
}

function Navbar({ user }) {
    const { token, setToken } = useToken();
    const { addToast } = useToasts()
    const history = useHistory();
    const location = useLocation();
    useEffect(() => {
        console.log(location.pathname); // result: '/secondpage'
        console.log(location.search); // result: '?query=abc'
        console.log(location.state.newStructure); // result: 'some_value'
    }, [location]);

    let text = user == null ? <div className="loader"></div> : user.username;

    const handleLogOut = async e => {
        const response = await logout({
            token
        });

        console.log(response)

        if (response.ok) {

            //if (response.status == 201) {
            addToast('Logout!', {
                appearance: 'success',
                autoDismiss: true,
                pauseonhover: "true",
            })
            //}

            history.push({
                pathname: "/login",
                state: {
                    response: "succesful logout"
                }
            });
            setToken(null)
        } else {
            if (response.status == 500) {
                addToast(response.body.details, {
                    appearance: 'error',
                    autoDismiss: true,
                    pauseonhover: "true",
                })
            }
            else {
                addToast('Por favor intente de nuevo', {
                    appearance: 'error',
                    autoDismiss: true,
                    pauseonhover: "true",
                })
            }

        }


    }

    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark" >
            <div className="container-fluid">
                <div className="navbar-header">

                    <a className="navbar-brand">PW II</a>
                </div>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">

                        <li className={getNavLinkClass("/game")}><NavLink className="nav-link" to="/game" >Game</NavLink></li>

                    </ul>
                </div>

                <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul className="navbar-nav">

                        <li className="nav-item dropdown mr-3">
                            <a className="nav-link dropdown-toggle text-white px-4 " href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {text}
                            </a>
                            <div className="dropdown-menu bg-dark " aria-labelledby="navbarDropdownMenuLink">
                                <a className="dropdown-item text-white bg-dark" href="#">Action</a>
                                <a className="dropdown-item text-white bg-dark" href="#">Another action</a>
                                <a className="dropdown-item text-white bg-dark" onClick={handleLogOut} >Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )

};
Navbar = withRouter(Navbar);
export default Navbar;