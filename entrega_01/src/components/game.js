import React, { useState, useEffect } from "react";
import Navbar from './navbar.js'
import useToken from '../useToken';
import Board from './board'
import { deck } from './cards'
import { startGame, getCardFromDeck } from './rules'
import '../styles/game.css'

function Game() {
    async function getUserData(token) {

        const response = await fetch('https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user', {
            method: 'GET',
            headers: {
                'Authorization': token
            }
        })
        const json = await response.json();
        return json;
    }


    const [user, setUser] = useState(null);
    const { token } = useToken();

    if (user == null) {

        getUserData(
            token
        ).then(user => setUser(user))
            .catch(error => {
                console.error(error);
            });
    }
    let text = user == null ? "player 1" : user.username;

    const [gameState, setGameState] = useState(startGame(text))

    const handleDeck = async e => {
        const s = getCardFromDeck(gameState)
        setGameState(s)
        console.log(gameState)
    }

    return (
        <div>
            <Navbar user={user} />
            <h1>Game</h1>
            <table id="tabla">
                <tr>
                    <td>
                        <Board gameState={gameState} />
                    </td>
                    <td>
                        <div className="play">
                            <h3 className="turn">
                                Turn:
            </h3>
                            <div className="deck">
                                <button type="button" onClick={handleDeck} id="deck">
                                    <img
                                        src="https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2Fback.png"
                                    />
                                </button>
                            </div>
                            <div className="playing">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

        </div>
    );

}

export default Game;