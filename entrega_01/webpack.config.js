const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
require("@babel/polyfill");
module.exports = {
    mode: 'development',
    entry: ["@babel/polyfill", './src/index.js'],
    devtool: 'inline-source-map',
    output: {
        filename: 'index.bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    //query: { presets: ["react"] },
                    options: {
                        presets: ['@babel/preset-react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    //redirect 404 to index
    devServer: {
        historyApiFallback: true,
    },
    plugins: [

        new HtmlWebpackPlugin({
            template: './public/index.html'
        })
    ]
};