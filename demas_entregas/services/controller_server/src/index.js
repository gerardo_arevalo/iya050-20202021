const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const logic = require('./logic.js')
const app = new Koa();
const router = new Router();

router.post("/game", async (ctx) => {
  let newGeneratedGame = await logic.generateGame()
  newGeneratedGame.player = ctx.request.body.player;
  // 1. post mutation to Stats server to store new game
  let newGame = await logic.createGame(newGeneratedGame);
  // 2. return state
  //console.log(newGame)
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: newGame.id,
    state: newGame
    // ...
  });
});

router.get("/game/:id", async (ctx) => {
  // 1. get current state from Stats server

  let state = await logic.getGame(ctx.params.id)
  // 2. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    result: state
    // ...
  });
});

router.post("/game/:id/event", async (ctx) => {
  // 1. get current state from Stats server
  let state = await logic.getGame(ctx.params.id);
  // 2. get next state from Game server
  let event = ctx.request.body.event;

  let newState;
  switch (event) {
    case 'add':
      newState = await logic.addCard(state);
      break;
    case 'playCard':
      let cardToPlay = ctx.request.body.cardToPlay;
      newState = await logic.playCard(state, cardToPlay);
      break;
    default:
      console.log('Lo lamentamos, por el momento no disponemos de ' + event + '.');
  }

  if (newState === 'Carta no jugable') {

    ctx.response.set("Content-Type", "application/json");
    ctx.body = JSON.stringify({
      id: ctx.params.id,
      state: newState
      // ...
    });
  } else {
    // 3. post mutation to Stats server to store next state
    let response = await logic.updateGame(newState);
    // 4. return state

    ctx.response.set("Content-Type", "application/json");
    ctx.body = JSON.stringify({
      id: ctx.params.id,
      state: response
      // ...
    });
  }


});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 5001);
