let graphql_url = process.env.GRAPHQL_SERVER_URL || "http://localhost:4000/";
let game_server_url = process.env.GAME_SERVER_URL || "http://localhost:3000/";
const fetch = require('node-fetch')
async function getGame(gameid) {

  const query = `query{
      game(id:"${gameid}"){        
          id
          
          playerCards{
            name
            url
            numbers
            colors
          }
          cpuCards{
            name
            url
            numbers
            colors
          }
          cardPlaying{
            name
            url
            numbers
            colors
          }
          playerOneTurn
          playerTwoTurn
          winner
      }
    }`;
  return await fetch(graphql_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => { return data.data.game });
};

async function generateGame() {
  let response = await fetch(`${game_server_url}game/new`)
    .then(r => r.json())
    .then(data => data);
  return response;
};

async function createGame(game) {
  let mutationInput = `{
    player: "${game.player}"
    playerTwoTurn: ${game.playerTwoTurn}
    playerOneTurn: ${game.playerOneTurn}
    playerCards: [
      ${game.playerCards.map(card => {
    return `{
        name: "${card.name}"
        numbers: [${card.numbers}]
        colors: [${card.colors.map(color => `"${color}"`)}]
        url: "${card.url}"
      }`})}
    ]
    cpuCards: [
      ${game.cpuCards.map(card => {
      return `{
        name: "${card.name}"
        numbers: [${card.numbers}]
        colors: [${card.colors.map(color => `"${color}"`)}]
        url: "${card.url}"
      }`})}
    ]
    cardPlaying: {
      name: "${game.cardPlaying.name}"
      numbers: [${game.cardPlaying.numbers}]
      colors: [${game.cardPlaying.colors.map(color => `"${color}"`)}]
      url: "${game.cardPlaying.url}"
    }

  }`
  const query = `mutation{
      createGame(input:${mutationInput}){
          id
         
          playerCards{
            name
            url
            numbers
            colors
          }
          cpuCards{
            name
            url
            numbers
            colors
          }
          cardPlaying{
            name
            url
            numbers
            colors
          }
          playerOneTurn
          playerTwoTurn
          winner
      }
    }`;
  return await fetch(graphql_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => { return data.data.createGame });
};

async function updateGame(game) {
  let mutationInput = `{
    id:"${game.id}"
    player: "${game.player}"
    playerTwoTurn: ${game.playerTwoTurn}
    playerOneTurn: ${game.playerOneTurn}
    winner: "${game.winner}"
    playerCards: [
      ${game.playerCards.map(card => {
    return `{
        name: "${card.name}"
        numbers: [${card.numbers}]
        colors: [${card.colors.map(color => `"${color}"`)}]
        url: "${card.url}"
      }`})}
    ]
    cpuCards: [
      ${game.cpuCards.map(card => {
      return `{
        name: "${card.name}"
        numbers: [${card.numbers}]
        colors: [${card.colors.map(color => `"${color}"`)}]
        url: "${card.url}"
      }`})}
    ]
    cardPlaying: {
      name: "${game.cardPlaying.name}"
      numbers: [${game.cardPlaying.numbers}]
      colors: [${game.cardPlaying.colors.map(color => `"${color}"`)}]
      url: "${game.cardPlaying.url}"
    }

  }`

  const query = `mutation{
      updateGame(input:${mutationInput}){
          id
          
          playerCards{
            name
            url
            numbers
            colors
          }
          cpuCards{
            name
            url
            numbers
            colors
          }
          cardPlaying{
            name
            url
            numbers
            colors
          }
          playerOneTurn
          playerTwoTurn
          winner
      }
    }`;

  return await fetch(graphql_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data.data.updateGame);
};

async function addCard(state) {

  let response = await fetch(`${game_server_url}game/card`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      state
    })
  })
    .then(r => r.json())
    .then(data => data);

  let stb = { "id": "cobar-game1", "player": { "username": "cobar" }, "playerCards": [{ "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }, { "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }, { "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }], "cpuCards": [{ "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }, { "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }, { "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }], "cardPlaying": { "name": "YellowThree", "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png", "numbers": [3], "colors": ["Yellow"] }, "winner": "" };

  return response;
};

async function playCard(state, cardToPlay) {

  let response = await fetch(`${game_server_url}game/play`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      state,
      cardToPlay
    })
  })
    .then(r => r.json())
    .then(data => data);

  return response;
};

exports.getGame = getGame;
exports.generateGame = generateGame;
exports.createGame = createGame;
exports.updateGame = updateGame;
exports.addCard = addCard;
exports.playCard = playCard;