// TBI
const { GraphQLServer } = require('graphql-yoga')
const fs = require('fs');
const path = require('path');
require('dotenv').config()
const user = require('./src/user.js')
const game = require('./src/game.js')
const card = require('./src/card.js')
const stats = require('./src/statistics.js')

const typeDefs = fs.readFileSync(
  path.join(__dirname, './src/schema.graphql'),
  'utf8'
)


const resolvers = {
  Query: {
    player: (_, { username }) => user.getUserInfo(username),
    game: (_, { id }) => game.getGameInfo(id),
    card: (_, { name }) => card.getCardInfo(name),
    gamesWin: (_, { username }) => stats.gamesWin(username),
    gamesLost: (_, { username }) => stats.gamesLost(username),
    gamesUnfinished: (_, { username }) => stats.gamesUnfinished(username),
    gameWithMaxPlayerNumberOfCards: (_, { username }) => stats.gameWithMaxPlayerNumberOfCards(username),
    gameWithMaxCpuNumberOfCards: (_, { username }) => stats.gameWithMaxPlayerNumberOfCards(username)
  },
  Mutation: {
    createPlayer: (_, { username }) => user.createPlayer(username),
    createGame: (_, { input }) => game.createGame(input),
    updateGame: (_, { input }) => game.updateGame(input)
  },
  Player: {
    username: (player) => player.value.username,
    gamesList: (player) => player.value.gamesList,
    games: player => user.getAllUserGames(player.value.username)
  },
  Card: {
    name: card => card.name,
    numbers: card => card.numbers,
    colors: card => card.colors,
    url: card => card.url
  },
  Game: {
    id: game => game.value.id,
    player: game => { return user.getUserInfo(game.value.player) },
    playerCards: game => game.value.playerCards,
    cpuCards: game => game.value.cpuCards,
    cardPlaying: game => game.value.cardPlaying,
    winner: game => game.value.winner,
    playerOneTurn: game => game.value.playerOneTurn,
    playerTwoTurn: game => game.value.playerTwoTurn,
    //lastModified: game => game.lastModified
  },
  Stat: {
    game: stat => stat.game,
    number: stat => stat.number
  }
}

const server = new GraphQLServer({ typeDefs, resolvers })

server.start({
  playground: '/',
  port: process.env.PORT
},
  () => {
    //Save cards from file in db
    //util.SaveAllCardsInDB();

    console.log('Server is running on localhost:' + process.env.PORT)
  }
)