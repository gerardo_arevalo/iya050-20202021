const fetch = require('node-fetch')
require('dotenv').config()
const util = require('./utilities.js')

async function getCardInfo(name) {
    const response = await fetch(`${process.env.PERSISTENCE_URL}/pairs/card-${name}/`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY }
    }).then(res => res.json()).then(res => { res.value = util.stringToObj(res.value); return res });

    return response;
};

async function getPlayerCardInfo(playerCards) {

    let cards = [];
    playerCards.forEach(card => {

    });
    return response;
};

exports.getCardInfo = getCardInfo;