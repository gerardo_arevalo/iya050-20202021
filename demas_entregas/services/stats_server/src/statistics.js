const fetch = require('node-fetch')
require('dotenv').config()
const user = require('./user.js')

async function gamesWin(username) {

    const games = await user.getAllUserGames(username);
    let number = games.filter(game => game.value.winner == username).length;

    return number;
};

async function gamesLost(username) {

    const games = await user.getAllUserGames(username);
    let number = games.filter(game => game.value.winner == "cpu").length;

    return number;
};

async function gamesUnfinished(username) {

    const games = await user.getAllUserGames(username);

    let number = games.filter(game => game.value.winner == '').length;

    return number;
};

async function gameWithMaxPlayerNumberOfCards(username) {

    const games = await user.getAllUserGames(username);
    let response = {
        number: 0,
        game: ""
    }
    games.forEach((game) => {
        let length = game.value.playerCards.length;
        if (response.number < length) {
            response.number = length
            response.game = game.value.id;
        }
    })

    return response;
};

async function gameWithMaxCpuNumberOfCards(username) {

    const games = await user.getAllUserGames(username);
    let response = {
        number: 0,
        game: ""
    }
    games.forEach((game) => {
        let length = game.value.cpuCards.length;
        if (response.number < length) {
            response.number = length
            response.game = game.value.id;
        }
    })

    return response;
};

exports.gamesWin = gamesWin;
exports.gamesLost = gamesLost;
exports.gamesUnfinished = gamesUnfinished;
exports.gameWithMaxPlayerNumberOfCards = gameWithMaxPlayerNumberOfCards;
exports.gameWithMaxCpuNumberOfCards = gameWithMaxCpuNumberOfCards;