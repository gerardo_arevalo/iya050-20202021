function stringToObj(str) {
    let jsonStr = str.replace(/(\w+:)|(\w+ :)/g, function (matchedStr) {
        return '"' + matchedStr.substring(0, matchedStr.length - 1) + '":';
    });
    //console.log(jsonStr)
    jsonStr = JSON.parse(jsonStr)
    return jsonStr;
};

//Solo se ejecutará en server.js una vez para que todas las cartas estén guardadas y 
//disponibles en bd y no en el archivo deck.js
function SaveAllCardsInDB() {
    const cards = require('./deck.js')
    const fetch = require('node-fetch')

    cards.deck.forEach(card => {
        fetch(`${process.env.PERSISTENCE_URL}/pairs/card-${card.name}/`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY },
            body: JSON.stringify(card)
        });

    });
};


exports.stringToObj = stringToObj;
exports.SaveAllCardsInDB = SaveAllCardsInDB;