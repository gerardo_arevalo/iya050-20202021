const fetch = require('node-fetch')
require('dotenv').config()
const util = require('./utilities.js')

async function getGameInfo(gameid) {

    const response = await fetch(`${process.env.PERSISTENCE_URL}/pairs/${gameid}/`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY }
    }).then(res => res.json()).then(res => { res.value = JSON.parse(res.value); return res });

    return response;
};

async function createGame(input) {
    //get all user games to create the id
    let numberOfGames = await fetch(`${process.env.PERSISTENCE_URL}/collections/${input.player}-game/`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY }
    }).then(res => res.json()).then(res => res.length);

    const id = numberOfGames + 1;
    const game = {
        id: `${input.player}-game${id}`,
        player: input.player,
        playerCards: input.playerCards,
        cpuCards: input.cpuCards,
        cardPlaying: input.cardPlaying,
        playerOneTurn: input.playerOneTurn,
        playerTwoTurn: input.playerTwoTurn,
        winner: ""
    }

    const response = await fetch(`${process.env.PERSISTENCE_URL}/pairs/${input.player}-game${id}/`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY },
        body: JSON.stringify(game)
    }).then(res => res.json()).then(res => { res.value = JSON.parse(res.value); return res });

    return response;
};
async function updateGame(input) {

    let game = getGameInfo(input.id);
    game.value = {
        id: input.id,
        player: input.player,
        playerCards: input.playerCards,
        cpuCards: input.cpuCards,
        cardPlaying: input.cardPlaying,
        playerOneTurn: input.playerOneTurn,
        playerTwoTurn: input.playerTwoTurn,
        winner: input.winner,

    }

    const response = await fetch(`${process.env.PERSISTENCE_URL}/pairs/${input.id}/`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY },
        body: JSON.stringify(game.value)
    }).then(res => res.json()).then(res => { res.value = JSON.parse(res.value); return res });

    return response;
};

exports.getGameInfo = getGameInfo;
exports.createGame = createGame;
exports.updateGame = updateGame;
