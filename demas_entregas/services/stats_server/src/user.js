const fetch = require('node-fetch')
require('dotenv').config()
const util = require('./utilities.js')

async function createPlayer(username) {
    let games = await getAllUserGames(username);
    let gamesList = games.map((game) => {
        return game.key;
    });
    const player = {
        username: username,
        gamesList: gamesList,
        games: []
    }
    const response = await fetch(`${process.env.PERSISTENCE_URL}/pairs/${username}/`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY },
        body: JSON.stringify(player)
    }).then(res => res.json()).then(res => { res.value = util.stringToObj(res.value); return res });

    return response;
};

async function getUserInfo(username) {

    return await fetch(`${process.env.PERSISTENCE_URL}/pairs/${username}/`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY }
    }).then(res => res.json()).then(res => { res.value = util.stringToObj(res.value); return res });
};

async function getAllUserGames(username) {

    return await fetch(`${process.env.PERSISTENCE_URL}/collections/${username}-game/`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'x-application-id': process.env.APP_KEY }
    }).then(res => res.json()).then(res => {

        res.forEach((x) => {
            x.value = JSON.parse(x.value);
        })
        return res;
    });
};
exports.getUserInfo = getUserInfo;
exports.getAllUserGames = getAllUserGames;
exports.createPlayer = createPlayer;