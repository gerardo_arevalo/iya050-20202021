const deck = [
    {
        name: "Draw4",
        numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        colors: ["Red", "Blue", "Yellow", "Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F5.png"
    },
    {
        name: "ChangeColor",
        numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        colors: ["Red", "Blue", "Yellow", "Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F4.png"
    },
    {
        name: "RedZero",
        numbers: [0],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F000.png"
    },
    {
        name: "RedOne",
        numbers: [1],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F001.png"
    },
    {
        name: "RedTwo",
        numbers: [2],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F002.png"
    },
    {
        name: "RedThree",
        numbers: [3],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F003.png"
    },
    {
        name: "RedFour",
        numbers: [4],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F004.png"
    },
    {
        name: "RedFive",
        numbers: [5],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F005.png"
    },
    {
        name: "RedSix",
        numbers: [6],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F006.png"
    },
    {
        name: "RedSeven",
        numbers: [7],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F007.png"
    },
    {
        name: "RedEight",
        numbers: [8],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F008.png"
    },
    {
        name: "RedNine",
        numbers: [9],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F009.png"
    },
    {
        name: "RedSkip",
        numbers: [10],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F010.png"
    },
    {
        name: "RedDraw2",
        numbers: [11],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F012.png"
    },
    {
        name: "RedReverse",
        numbers: [12],
        colors: ["Red"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F011.png"
    },
    {
        name: "BlueZero",
        numbers: [0],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F300.png"
    },
    {
        name: "BlueOne",
        numbers: [1],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F301.png"
    },
    {
        name: "BlueTwo",
        numbers: [2],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F302.png"
    },
    {
        name: "BlueThree",
        numbers: [3],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F303.png"
    },
    {
        name: "BlueFour",
        numbers: [4],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F304.png"
    },
    {
        name: "BlueFive",
        numbers: [5],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F305.png"
    },
    {
        name: "BlueSix",
        numbers: [6],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F306.png"
    },
    {
        name: "BlueSeven",
        numbers: [7],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F307.png"
    },
    {
        name: "BlueEight",
        numbers: [8],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F308.png"
    },
    {
        name: "BlueNine",
        numbers: [9],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F309.png"
    },
    {
        name: "BlueSkip",
        numbers: [10],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F310.png"
    },
    {
        name: "BlueDraw2",
        numbers: [11],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F312.png"
    },
    {
        name: "BlueReverse",
        numbers: [12],
        colors: ["Blue"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F311.png"
    },
    {
        name: "YellowZero",
        numbers: [0],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F100.png"
    },
    {
        name: "YellowOne",
        numbers: [1],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F101.png"
    },
    {
        name: "YellowTwo",
        numbers: [2],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F102.png"
    },
    {
        name: "YellowThree",
        numbers: [3],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
    },
    {
        name: "YellowFour",
        numbers: [4],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F104.png"
    },
    {
        name: "YellowFive",
        numbers: [5],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F105.png"
    },
    {
        name: "YellowSix",
        numbers: [6],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F106.png"
    },
    {
        name: "YellowSeven",
        numbers: [7],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F107.png"
    },
    {
        name: "YellowEight",
        numbers: [8],
        colors: ["Yellow"],
        url:
            "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F108.png?v=1610788463174"
    },
    {
        name: "YellowNine",
        numbers: [9],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F109.png"
    },
    {
        name: "YellowSkip",
        numbers: [10],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F110.png"
    },
    {
        name: "YellowDraw2",
        numbers: [11],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F112.png"
    },
    {
        name: "YellowReverse",
        numbers: [12],
        colors: ["Yellow"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F111.png"
    },
    {
        name: "GreenZero",
        numbers: [0],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F200.png"
    },
    {
        name: "GreenOne",
        numbers: [1],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F201.png"
    },
    {
        name: "GreenTwo",
        numbers: [2],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F202.png"
    },
    {
        name: "GreenThree",
        numbers: [3],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F203.png"
    },
    {
        name: "GreenFour",
        numbers: [4],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F204.png"
    },
    {
        name: "GreenFive",
        numbers: [5],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F205.png"
    },
    {
        name: "GreenSix",
        numbers: [6],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F206.png"
    },
    {
        name: "GreenSeven",
        numbers: [7],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F207.png"
    },
    {
        name: "GreenEight",
        numbers: [8],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F208.png"
    },
    {
        name: "GreenNine",
        numbers: [9],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F209.png"
    },
    {
        name: "GreenSkip",
        numbers: [10],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F210.png"
    },
    {
        name: "GreenDraw2",
        numbers: [11],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F212.png"
    },
    {
        name: "GreenReverse",
        numbers: [12],
        colors: ["Green"],
        url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F211.png"
    }
];
exports.deck = deck;
//export { deck };