// 📝
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const gameRoutes = require("./src/routes/unoGame.js");
const port = process.env.PORT || 3000;
const server = express();
server.use(bodyParser.json());

server.use(cors());

server.get("/hello", (req, res) => {
    res.send("Hello World");
});

server.post("/echo", (req, res) => {

    res.json(req.body);
});

server.use("/game", gameRoutes);

server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});