const { expect } = require("chai");
const { isValidCard, playTurn } = require("../services/uno.js");

describe("Uno Game Logic", () => {
    describe("isValidCard", () => {
        const cardPlaying = {
            "name": "YellowSix",
            "numbers": [
                6
            ],
            "colors": [
                "Yellow"
            ],
            "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F106.png"
        };
        describe("given a valid color card to play and the current card playing", () => {
            it("should return true", () => {
                const validCardToPlay = {
                    "name": "YellowThree",
                    "numbers": [
                        3
                    ],
                    "colors": [
                        "Yellow"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
                };

                const expected = true;
                const actual = isValidCard(validCardToPlay, cardPlaying)

                expect(actual).to.deep.equal(expected);
            });
        });
        describe("given a invalid color card to play and the current card playing", () => {
            it("should return false", () => {
                const validCardToPlay = {
                    "name": "BlueThree",
                    "numbers": [
                        3
                    ],
                    "colors": [
                        "Blue"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
                };

                const expected = false;
                const actual = isValidCard(validCardToPlay, cardPlaying)

                expect(actual).to.deep.equal(expected);
            });
        });
    });
    describe("playTurn", () => {
        const state = {
            "playerOneCards": [
                {
                    "name": "RedReverse",
                    "numbers": [
                        12
                    ],
                    "colors": [
                        "Red"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F011.png"
                },
                {
                    "name": "YellowThree",
                    "numbers": [
                        3
                    ],
                    "colors": [
                        "Yellow"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
                },

                {
                    "name": "RedSeven",
                    "numbers": [
                        7
                    ],
                    "colors": [
                        "Red"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F007.png"
                },
                {
                    "name": "RedSix",
                    "numbers": [
                        6
                    ],
                    "colors": [
                        "Red"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F006.png"
                }
            ],
            "playerTwoCards": [
                {
                    "name": "GreenDraw2",
                    "numbers": [
                        11
                    ],
                    "colors": [
                        "Green"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F212.png"
                },
                {
                    "name": "BlueSix",
                    "numbers": [
                        6
                    ],
                    "colors": [
                        "Blue"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F306.png"
                },
                {
                    "name": "YellowThree",
                    "numbers": [
                        3
                    ],
                    "colors": [
                        "Yellow"
                    ],
                    "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
                }
            ],
            "playerOneTurn": true,
            "playerTwoTurn": false,
            "cardPlaying": {
                "name": "RedSix",
                "numbers": [
                    6
                ],
                "colors": [
                    "Red"
                ],
                "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F006.png"
            }
        };
        const cardToPlay = {
            "name": "RedReverse",
            "numbers": [
                12
            ],
            "colors": [
                "Red"
            ],
            "url": "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F011.png"
        };
        describe("given a current state and a card to play", () => {
            it("should return a new state with the new card playing same as the card played", () => {
                let currentState = JSON.parse(JSON.stringify(state));
                const expected = cardToPlay;
                const newState = playTurn(currentState, cardToPlay)
                expect(newState.cardPlaying).to.deep.equal(expected);
            });
            it("should remove the card played from the player cards", () => {
                let currentState = JSON.parse(JSON.stringify(state));
                const expected = (currentState.playerOneCards.length) - 1;
                const newState = playTurn(currentState, cardToPlay)
                expect(newState.playerOneCards.length).to.deep.equal(expected);
            });
            it("should change the players turns", () => {
                let currentState = JSON.parse(JSON.stringify(state));
                let playerOneExpected = !currentState.playerOneTurn;
                let playerTwoExpected = !currentState.playerTwoTurn;
                const newState = playTurn(currentState, cardToPlay)
                expect(newState.playerOneTurn).to.deep.equal(playerOneExpected);
                expect(newState.playerTwoTurn).to.deep.equal(playerTwoExpected);
            });

        });
    });
});
