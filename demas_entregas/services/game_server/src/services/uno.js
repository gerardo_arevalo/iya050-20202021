const { deck } = require('./cards');

const isValidCard = (cardToPlay, cardPlaying) => {

    return (cardPlaying.numbers.some(i => cardToPlay.numbers.includes(i)) ||
        cardPlaying.colors.some(i => cardToPlay.colors.includes(i)))
        ? true : false;
};

const getRandomCard = () => {
    return deck[Math.floor(Math.random() * deck.length)]
}

const validStartCard = (item = getRandomCard()) => {
    while (
        (item.name === "Draw4") ||
        (item.name === "ChangeColor") ||
        (item.name === "RedSkip") ||
        (item.name === "RedDraw2") ||
        (item.name === "RedReverse") ||
        (item.name === "BlueSkip") ||
        (item.name === "BlueDraw2") ||
        (item.name === "BlueReverse") ||
        (item.name === "YellowSkip") ||
        (item.name === "YellowDraw2") ||
        (item.name === "YellowReverse") ||
        (item.name === "GreenSkip") ||
        (item.name === "GreenDraw2") ||
        (item.name === "GreenReverse")
    ) {
        item = getRandomCard();
    }
    return item;
};

const newGame = () => {
    let state = {};
    state.playerCards = Array.from({ length: 7 }, getRandomCard);
    state.cpuCards = Array.from({ length: 7 }, getRandomCard);
    state.playerOneTurn = true;
    state.playerTwoTurn = false;
    state.winner = "";
    state.cardPlaying = validStartCard();
    return state;
}
const specialCard = (state, cardToPlay) => {
    switch (cardToPlay.name) {
        case 'Draw4':
            for (var i = 0; i < 4; i++) {
                state = getCardFromDeck(state);
            }
            break;
        case 'playCard':
            let cardToPlay = ctx.request.body.cardToPlay;

            break;
        default:

    }
    return state;
}

const playTurn = (currentState, cardToPlay) => {
    if (isValidCard(cardToPlay, currentState.cardPlaying)) {
        //let newState =Object.assign({}, currentState);
        let newState = JSON.parse(JSON.stringify(currentState));
        newState.cardPlaying = cardToPlay;
        if (newState.playerOneTurn) {
            newState.playerCards.splice(
                newState.playerCards.findIndex(card => card.name === cardToPlay.name), 1);
            newState.playerOneTurn = !newState.playerOneTurn
            newState.playerTwoTurn = !newState.playerTwoTurn
            newState = specialCard(newState, cardToPlay)
        } else {
            newState.cpuCards.splice(
                newState.cpuCards.findIndex(card => card.name === cardToPlay.name), 1);

            newState.playerOneTurn = !newState.playerOneTurn
            newState.playerTwoTurn = !newState.playerTwoTurn
            newState = specialCard(newState, cardToPlay)
        }
        return newState;
    } else {
        return "Carta no jugable";
    }

}

const getCardFromDeck = (state) => {

    let newState = JSON.parse(JSON.stringify(state));
    if (state.playerOneTurn) {
        newState.playerCards.push(getRandomCard());

    } else {
        newState.cpuCards.push(getRandomCard());
    }
    // newState.playerOneTurn = !state.playerOneTurn;
    // newState.playerTwoTurn = !state.playerTwoTurn;

    return newState
}

module.exports = { isValidCard, validStartCard, newGame, playTurn, getCardFromDeck };