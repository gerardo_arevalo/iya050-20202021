const uno = require("../services/uno.js");

const play = (req, res) => {
    res.json(uno.playTurn(req.body.state, req.body.cardToPlay));
};

const game = (req, res) => {
    res.json(uno.newGame());
};

const card = async (req, res) => {
    res.json(await uno.getCardFromDeck(req.body.state));
};

module.exports = { play, game, card };