/* eslint-env mocha */
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const fetch = require("node-fetch");
chai.use(chaiAsPromised);
const expect = chai.expect;
const API_URL = "https://winter-fluorescent-bed.glitch.me/"
describe("server", () => {
    describe("test server is working correctly", () => {
        describe("fetching GET /hello", () => {
            it('should have status 200 and return "Hello World" ', async () => {
                const expected = "Hello World"
                let response = await fetch(`${API_URL}/hello`)
                expect(response.status).to.equal(200);
                const message = await response.text();
                expect(message).to.equal(expected);
            });
        });
    });
    describe("create a new game", () => {
        describe("fetching GET /game/new", async () => {

            it('should return a JSON object ', async () => {
                let response = await fetch(`${API_URL}/game/new`);
                var game = await response.json();
                expect(game).to.be.an('Object');
            });
            it('should be the turn of playerOne ', async () => {
                let response = await fetch(`${API_URL}/game/new`);
                var game = await response.json();
                let playerOneTurn = game.playerOneTurn;
                expect(playerOneTurn).to.equal(true);
            });
            it('players should have 7 cards', async () => {
                let response = await fetch(`${API_URL}/game/new`);
                var game = await response.json();
                let playerOneCards = game.playerOneCards.length;
                let playerTwoCards = game.playerTwoCards.length;
                expect(playerOneCards).to.equal(7);
                expect(playerTwoCards).to.equal(7);
            });
        });
    });
});
