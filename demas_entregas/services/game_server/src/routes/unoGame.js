const express = require("express");
const unoGameController = require("../controllers/unoGame.js");

const router = express.Router();

router.get("/new", unoGameController.game);
router.post("/card", unoGameController.card);
router.post("/play", unoGameController.play);

module.exports = router;