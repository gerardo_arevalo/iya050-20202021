let url_graphql = process.env.GRAPHQL_SERVER_URL || "http://localhost:4000/";

export async function playerExists(username) {
  const query = `query{
    player(username:"${username}"){
      username
    }
  }`;
  try {
    return fetch(url_graphql, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query,
      })
    })
      .then(r => r.json())
      .then(data => data.data.player);
  } catch (err) {
    playerExists(username)
  }
};

export function createPlayer(username) {
  const query = `mutation{
    createPlayer(username:"${username}"){
      username 
    }
  }`;
  try {
    return fetch(url_graphql, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query,
      })
    })
      .then(r => r.json())
      .then(data => data.data.player);
  }
  catch (err) {
    createPlayer(username)
  }
};

export async function getPlayerGames(username) {
  const query = `query{
      player(username:"${username}"){
        gamesList
        games{
          id
          player{
            username
          }
          playerCards{
            name
          }
          cpuCards{
            name
          }
          cardPlaying{
            name
          }
          winner
        }
      }
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data.data.player);
};

export async function getGamesWin(username) {
  const query = `query{
      gamesWin(username:"${username}")
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data !== null ? data.data.gamesWin : data);
};

export async function getGamesLost(username) {
  const query = `query{
      gamesLost(username:"${username}")
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data.data.gamesLost);
};

export async function getGamesUnfinished(username) {
  const query = `query{
      gamesUnfinished(username:"${username}")
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data !== null ? data.data.gamesUnfinished : data);
};

export async function gameWithMaxPlayerNumberOfCards(username) {
  const query = `query{
      gameWithMaxPlayerNumberOfCards(username:"${username}"){
        game
        number
      }
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data !== null ? data.data.gameWithMaxPlayerNumberOfCards : data);
};

export async function gameWithMaxCpuNumberOfCards(username) {
  const query = `query{
      gameWithMaxCpuNumberOfCards(username:"${username}"){
        game
        number
      }
    }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data !== null ? data.data.gameWithMaxCpuNumberOfCards : data);
};

export async function getGame(gameid) {

  const query = `query{
    game(id:"${gameid}"){
      
        id
        player{
          username
        }
        playerCards{
          name
          url
        }
        cpuCards{
          name
          url
        }
        cardPlaying{
          name
          url
        }
        winner
    }
  }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data.data.game);
};

export async function createGame(game) {

  const query = `mutation{
    createGame(input:${game}){
        id
        player{
          username
        }
        playerCards{
          name
          url
        }
        cpuCards{
          name
          url
        }
        cardPlaying{
          name
          url
        }
        winner
    }
  }`;
  return await fetch(url_graphql, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query,
    })
  })
    .then(r => r.json())
    .then(data => data);
};
