let koa_api = process.env.CONTROLLER_SERVER_URL || "http://localhost:5001/";

export async function getGame(gameid) {
  try {
    let response = await fetch(`${koa_api}game/${gameid}`)
      .then(r => r.json())
      .then(data => data.result);
    return response;
  } catch (err) {
    return 'error'
  }
};

export async function createGame(player) {
  try {
    return await fetch(`${koa_api}game`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        player,
      })
    })
      .then(r => r.json())
      .then(data => data);
  } catch (err) {
    console.log("err")
    console.log(err)
    return "error"
  }
};

export async function addCard(gameid) {

  try {
    return await fetch(`${koa_api}game/${gameid}/event`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        "event": "add",
      })
    })
      .then(r => r.json())
      .then(data => data);
  } catch (err) {
    return "error";
  }
};
export async function playCard(gameid, cardToPlay) {
  try {
    return await fetch(`${koa_api}game/${gameid}/event`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        "event": "playCard",
        "cardToPlay": cardToPlay
      })
    })
      .then(r => r.json())
      .then(data => data);
  } catch (err) {
    console.log(err)
    return "error";
  }
};
