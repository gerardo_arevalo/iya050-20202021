import React, { useState, useContext, useEffect } from "react";
import { Link } from 'react-router-dom';
import AuthContext from "../AuthContext.js";
import * as utils from "../utilities.js"
import '../styles/history.css'
import '../styles/loading.css'
const History = () => {
  const { user } = useContext(AuthContext);
  const [games, setGames] = useState(null);

  useEffect(async () => {
    let result = await utils.getPlayerGames(user.name)
    console.log(result)
    setGames(result)

  }, []);

  checkNotNullElement();

  async function checkNotNullElement() {

    let goodGames = null;
    if (games.games.some((game) => game == null)) {
      goodGames = games.games.filter(game => game !== null);

      let goodGamesId = goodGames.map(game => game.id)
      let badGames = games.gamesList.filter(x => !goodGamesId.includes(x));
      while (true) {
        let good = goodGames.length;
        let list = games.gamesList.length;
        if (good < list) {
          for (let game of badGames) {
            let res = await utils.getGame(game);

            if (res !== null) {
              goodGames.push(res)
            }
          }
        } else {
          break;
        }
      }

      setGames(goodGames);
    } else {
      setGames(games.games);
    }


  }

  let text = games === null ? <div className="lds-facebook"><div></div><div></div><div></div></div> :
    games.length > 0 ? <table>
      <thead>
        <tr>
          <th>Player</th>
          <th>Active</th>
          <th>Url</th>
        </tr>
      </thead>
      <tbody>{games.map((game, index) => {
        return <tr key={index}><td>{game.id}</td><td>{game.active}</td><td>
          <Link style={{ textDecoration: 'none' }} to={`/game/play/${game.id}`}>Ver partida</Link></td></tr>
      })}</tbody>
    </table> : "No hay partidas";

  return (
    <div>
      <h1>{user.name}’s past games</h1>

      {text}

    </div>
  );
};

export default History;
