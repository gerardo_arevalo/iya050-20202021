import React, { useState, useContext, useEffect } from "react";
import { Link } from 'react-router-dom';
import { useHistory, useLocation, useRouteMatch, } from "react-router-dom";
import * as logic from "../gameLogic.js"
import AuthContext from "../AuthContext.js";
import '../styles/loading.css'
import '../styles/play.css'
import ModalBase from "./ModalBase.jsx";
const Play = (props) => {

  let history = useHistory();
  const [game, setGame] = useState();
  const [modalContent, setModalContent] = useState();
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const { user } = useContext(AuthContext);
  const routeMatch = useRouteMatch("/game/play/:gameId");
  const fakeGame = `{
    player: "${user.name}"
    playerCards: [
      {
        name: "YellowThree"
          numbers: [3]
          colors: ["Yellow"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
      }
        {
        name: "GreenNine"
          numbers: [9]
          colors: ["Green"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F209.png"
      }
        {
        name: "BlueSeven"
          numbers: [7]
          colors: ["Blue"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F307.png"
      }
    ]
    cpuCards: [
      {
        name: "YellowThree"
          numbers: [3]
          colors: ["Yellow"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
      }
        {
        name: "YellowThree"
          numbers: [3]
          colors: ["Yellow"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
      }
        {
        name: "YellowThree"
          numbers: [3]
          colors: ["Yellow"]
          url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
      }
    ]
    cardPlaying: {
      name: "YellowThree"
      numbers: [3]
      colors: ["Yellow"]
      url: "https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2F103.png"
    }
  
    active: false
  }`
  const location = useLocation();
  useEffect(async () => {

    if (routeMatch.params.gameId !== '1') {

      let result = await logic.getGame(routeMatch.params.gameId)
      setGame(result)
    }
  }, [routeMatch.params.gameId]);

  async function handleCreateClick() {
    setLoading(true)
    let response = await logic.createGame(user.name).then(res => res);

    if (response !== 'error') {
      setGame(JSON.parse(JSON.stringify(response.state)));
      setLoading(false)
      history.push(`/game/play/${response.id}`);
    } else {
      setLoading(false)
      setModalContent(<div className="modal"><div className="contenido">Lo sentimos, ocurrió un error. Por favor intenta de nuevo :c</div></div>)
      setOpen(true)
    }
  }

  async function handleAddClick() {
    let response = await logic.addCard(game.id);
    if (response.state.cardPlaying.name === 'Draw4') {
      setModalContent(<div className="modal">
        <div className="contenido">Toma 4 cartas XD</div>
        <img className="gif" src="https://media.tenor.com/images/e0a12a9bd57d7cabb9f732710fb2d13c/tenor.gif" alt="LOL"></img>
      </div>)
      setOpen(true)
      setGame(response.state);
    } else if (response !== 'error') {
      setGame(response.state);
    } else {
      handleAddClick()
    }
  }

  async function handlePlayClick(player, cardToPlay) {
    if ((player === 'player' && game.playerOneTurn) || (player === 'cpu' && game.playerTwoTurn)) {
      let response = await logic.playCard(game.id, cardToPlay).then(res => res);

      if (response.state === 'Carta no jugable') {
        setModalContent(<div className="modal"><div className="contenido">No puedes jugar esta carta!!</div></div>)
        setOpen(true)
      } else if (response === 'error') {
        handlePlayClick(player, cardToPlay);
      } else {
        if (response.state.playerCards.length === 0 || response.state.cpuCards.length === 0) {
          setModalContent(<div className="modal">
            <div className="contenido">Ganaste {player}!!</div>
            <img className="gif" src="https://cdn.glitch.com/91029bb1-da41-4906-8fcf-01dfcb7a705f%2Fpato.gif?v=1622470948815" alt="WIN"></img>
          </div>)
          setOpen(true)
        }
        setGame(response.state);
      }
    } else {
      setModalContent(<div className="modal"><div className="contenido">No es tu turno!!</div></div>)
      setOpen(true)
    }
  }

  const handleClose = () => {
    setOpen(false);
  };

  let viewGameInfo = () => {

    return game !== null ? <div>
      <ModalBase open={open} handleClose={handleClose} modalBody={modalContent} />
      <div className="tablero"><h2>{game.id}</h2>
        <h2>Turno: {game.playerOneTurn ? "Jugador 1" : "CPU"}</h2></div>
      <div className="tablero">
        <div className="cartas">
          <h3>Cartas del jugador:</h3>
          <div className="container">
            {game.playerCards.map((card, index) => (
              <img className="item carta" onClick={() => { handlePlayClick("player", card) }} key={`player-${index}`} src={card.url} alt={card.name}></img>
            ))}
          </div>
          <h3>Cartas de la cpu:</h3>
          <div className="container">{game.cpuCards.map((card, index) => (
            <img className="item carta" onClick={() => { handlePlayClick("cpu", card) }} key={`cpu-${index}`} src={card.url} alt={card.name}></img>
          ))}</div>
        </div>
        <div className="actual">
          <h3>Ganador: {game.winner !== "" ? game.winner : "Partida no finalizada"}</h3>
          <div className="mazo">
            <div>
              <h3>Carta actual</h3>
              <img className="item" src={game.cardPlaying.url} alt={game.cardPlaying.name}></img>
            </div>
            <div>
              <h3>Mazo</h3>
              <img onClick={handleAddClick} className="item carta" src="https://cdn.glitch.com/19fc1ca0-3be8-4954-b5eb-75fa38fb07d5%2Fback.png" ></img>
            </div>
          </div>
        </div>

      </div >
    </div > : <div className="lds-facebook"><div></div><div></div><div></div></div>
  }


  let text = routeMatch.params.gameId === '1' ? <><button onClick={handleCreateClick} >Crear nueva partida</button>
  </> : viewGameInfo();

  return (
    <>
      <div>{text}</div>
      {loading ? <div className="lds-facebook"><div></div><div></div><div></div></div> : <></>}
    </>
  )
};

export default Play;
