import React, { useState, useContext, useEffect } from "react";
import AuthContext from "../AuthContext.js";
import * as utils from "../utilities.js"
import '../styles/loading.css'
import { Chart } from "react-google-charts";
const Stats = () => {
  const { user } = useContext(AuthContext);
  const [games, setGames] = useState(null);
  const [gamesWin, setGamesWin] = useState(null);
  const [gamesLost, setGamesLost] = useState(null);
  const [gamesUnfinished, setGamesUnfinished] = useState(null);
  const [gamePlayerMaxCards, setGamePlayerMaxCards] = useState(null);
  const [gameCpuMaxCards, setGameCpuMaxCards] = useState(null);

  useEffect(async () => {
    let result = await utils.getPlayerGames(user.name);

    result !== null ? setGames(result.games) : setGames(result)
  }, []);

  useEffect(async () => {
    let result = await utils.getGamesWin(user.name)

    setGamesWin(result)
  }, []);

  useEffect(async () => {
    let result = await utils.getGamesLost(user.name)

    setGamesLost(result)
  }, []);

  useEffect(async () => {
    let result = await utils.getGamesUnfinished(user.name)

    setGamesUnfinished(result)
  }, []);

  useEffect(async () => {
    let result = await utils.gameWithMaxPlayerNumberOfCards(user.name)

    setGamePlayerMaxCards(result)
  }, []);

  useEffect(async () => {
    let result = await utils.gameWithMaxCpuNumberOfCards(user.name)

    setGameCpuMaxCards(result)
  }, []);

  let gamesText = games !== null ? <h3>{games.length}</h3> : <div className="lds-facebook"><div></div><div></div><div></div></div>;

  let statsText = (gamesWin !== null && gamesLost !== null && gamesUnfinished !== null) ?
    ((gamesWin == 0 && gamesLost == 0 && gamesUnfinished == 0) ? "No hay partidas" :
      <Chart
        width={'500px'}
        height={'300px'}
        chartType="PieChart"
        loader={<div>Loading Chart</div>}
        data={[
          ['Estado', 'Cantidad'],
          ['Win', gamesWin],
          ['Lost', gamesLost],
          ['Unfinished', gamesUnfinished],
        ]}
        options={{
          title: '',
          sliceVisibilityThreshold: 0
        }}
        rootProps={{ 'data-testid': '1' }}
      />) : <div className="lds-facebook"><div></div><div></div><div></div></div>;

  let playerMaxText = gamePlayerMaxCards !== null ? <div><h3>{gamePlayerMaxCards.game}</h3><h3>Cantidad: {gamePlayerMaxCards.number}</h3></div> :
    <div className="lds-facebook"><div></div><div></div><div></div></div>;

  let cpuMaxText = gameCpuMaxCards !== null ? <div><h3>{gameCpuMaxCards.game}</h3><h3>Cantidad: {gameCpuMaxCards.number}</h3></div> :
    <div className="lds-facebook"><div></div><div></div><div></div></div>;
  return <div>
    <p>{user.name}’s stats</p>
    <h2>Numero de partidas</h2>
    {gamesText}
    <h2>Numero de partidas ganadas</h2>
    {statsText}

    <h2>Partida en el que el jugador ha tenido más cartas</h2>
    {playerMaxText}
    <h2>Partida en el que el cpu ha tenido más cartas</h2>
    {cpuMaxText}
  </div>;
};

export default Stats;
